.. Learn_More_Study_Less documentation master file, created by
   sphinx-quickstart on Mon Dec 16 13:47:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Learn_More_Study_Less's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: hello

Indices and tables
==================

Example site:http://tang.lichee.pro/fpga/display/av.html

And the source code: https://github.com/Lichee-Pi/Tang_Doc_Backup

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
